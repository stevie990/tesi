<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<title>RDF Converter</title>
	<script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
	<script type="text/javascript" src="js/main.js"></script>
	<link href="css/main.css" rel="stylesheet" type="text/css">
</head>
<body>

<form name="input" action="" method="get">
	<label for="spreadsheetURL">Spreadsheet address:</label>
	<input id="spreadsheetURL" type="text" name="spreadsheetURL">
	<input type="button" value="RDF" id="submitButton" onclick="getRDF()">
<!-- 	<button type="button" id="submitButton" onclick="getRDF()">RDF</button> -->
</form> 

<textarea id="textSpread" rows="40" cols="150" readonly></textarea>

</body>
</html>