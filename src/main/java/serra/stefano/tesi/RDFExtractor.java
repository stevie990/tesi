package serra.stefano.tesi;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletContext;

import spark.Request;
import spark.Response;
import spark.Route;
import spark.Spark;
import spark.servlet.SparkApplication;

import com.google.gdata.client.spreadsheet.SpreadsheetService;
import com.google.gdata.data.spreadsheet.CellEntry;
import com.google.gdata.data.spreadsheet.CellFeed;
import com.google.gdata.data.spreadsheet.SpreadsheetEntry;
import com.google.gdata.data.spreadsheet.SpreadsheetFeed;
import com.google.gdata.data.spreadsheet.WorksheetEntry;
import com.google.gdata.data.spreadsheet.WorksheetFeed;
import com.google.gdata.util.AuthenticationException;
import com.google.gdata.util.ServiceException;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.Resource;

public class RDFExtractor implements SparkApplication {

	@Override
	public void init() {
//	        
//		 Spark.get(new Route("/rdf")
//	        {
//
//	            @Override
//	            public Object handle(Request request, Response response)
//	            {
//	            	try {
//	            		String spreadsheetURL = request.queryParams("spreadsheet");//Arriva il link
//	            		spreadsheetURL = URLDecoder.decode(spreadsheetURL, "UTF-8");//Decodifica
//	            		RdfSpreadsheet rdfSpreadsheet = getFeed(spreadsheetURL);//Gli si passa il link e restituisce la classe con i valori
//	            		if(rdfSpreadsheet == null){
//	            			return "Spreadsheet non trovato nel feed";
//	            		}
//	            		//String rdfXml= serializeRdfSpreadsheet(rdfSpreadsheet);// Trasforma in rdf. Rende una stringa
//	            		String rdfXml = createRdf(rdfSpreadsheet);
//	            		
//						return rdfXml;
//					} catch (AuthenticationException e) {
//						e.printStackTrace();
//					} catch (MalformedURLException e) {
//						e.printStackTrace();
//					} catch (IOException e) {
//						e.printStackTrace();
//					} catch (ServiceException e) {
//						e.printStackTrace();
//					} catch (URISyntaxException e) {
//						e.printStackTrace();
//					}
//					return "Errore";
//	            }
//	        });
		 
//		 Spark.get(new Route("/rdf/:spreadsheet")
//	        {
//
//	            @Override
//	            public Object handle(Request request, Response response)
//	            {
//	            	try {
//	            		String spreadsheetURL = request.params(":spreadsheet");//Arriva il link
//	            		spreadsheetURL = URLDecoder.decode(spreadsheetURL, "UTF-8");//Decodifica
//	            		RdfSpreadsheet rdfSpreadsheet = getFeed(spreadsheetURL);//Gli si passa il link e restituisce la classe con i valori
//	            		if(rdfSpreadsheet == null){
//	            			return "Spreadsheet non trovato nel feed";
//	            		}
//	            		String rdfXml= serializeRdfSpreadsheet(rdfSpreadsheet);// Trasforma in rdf. Rende una stringa
//						return rdfXml;
//					} catch (AuthenticationException e) {
//						e.printStackTrace();
//					} catch (MalformedURLException e) {
//						e.printStackTrace();
//					} catch (IOException e) {
//						e.printStackTrace();
//					} catch (ServiceException e) {
//						e.printStackTrace();
//					} catch (URISyntaxException e) {
//						e.printStackTrace();
//					}
//					return "Errore";
//	            }
//	        });
		 
		 Spark.get(new Route("/rdf/:author/:spreadsheet")
	        {
	            @Override
	            public Object handle(Request request, Response response)
	            {
	            	try {
	            		Properties properties = new Properties();
	            		InputStream fis = null;
	            		fis = getClass().getClassLoader().getResourceAsStream("config.properties");
	            		if(fis == null){
	            			System.out.println("Cannot retrieve configuration file");
	            			return null;
	            		}
	            		properties.load(fis);
	            		String webRoot = properties.getProperty("webapp.root");
	            		String serverAccount = properties.getProperty("serverAccount");
	            		String passwordAccount = properties.getProperty("passwordAccount");
	            		
	            		String spreadsheetURL = request.params(":spreadsheet");//Arriva il link
	            		String author = request.params(":author");//Arriva il link
	            		spreadsheetURL = URLDecoder.decode(spreadsheetURL, "UTF-8");//Decodifica
	            		ElementList data = getFeed(spreadsheetURL, author, webRoot, serverAccount, passwordAccount);
	            		String rdfXml = createRdf(data, webRoot);
	            		return rdfXml;
					} catch (AuthenticationException e) {
						e.printStackTrace();
					} catch (MalformedURLException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ServiceException e) {
						e.printStackTrace();
					} catch (URISyntaxException e) {
						e.printStackTrace();
					}
					return "Errore";
	            }
	        });
	}
	
	private String createRdf(ElementList elementList, String webRoot){
			// create an empty Model
			Model model = ModelFactory.createDefaultModel();
			// create the only resource that we have in the spreadsheet. The title.
			Resource spreadSheetTitle = model.createResource( webRoot + elementList.getAuthor() + "/" + elementList.getTitle());
//			Resource spreadSheetTitle = model.createResource( "http://localhost:8081/Tesi/" + elementList.getAuthor() + "/" + elementList.getTitle());
//			Add property. In the spreadsheet we can have many properties. Le proprietà vanno aggiunte alla risorsa.
			List<XMLEntry> elements = elementList.getElements();
			
			for (XMLEntry tripleElement : elements) {
				Property property = model.createProperty(tripleElement.propertyNS + tripleElement.propertyValue);
				
				Resource resource = model.createResource(tripleElement.value);//tripleElement.propertyNS
				
				if(tripleElement.value.startsWith("http")){
					model.add(spreadSheetTitle, property, resource);
				}
				else
					model.add(spreadSheetTitle, property, tripleElement.value);
			}
			
//			ByteArrayOutputStream os = new ByteArrayOutputStream();
//			RDFWriter w = model.getWriter("RDF/XML");
			StringWriter sw = new StringWriter();
			sw.append("<?xml version='1.0' encoding='UTF-8'?>");
			model.write(sw, "RDF/XML");
//			String rdfXml = new String(os.toByteArray());
			return sw.toString();
		
		}
	
	public ElementList getFeed(String spreadsheetName) throws AuthenticationException, MalformedURLException, IOException, ServiceException, URISyntaxException {
		return getFeed(spreadsheetName, null, null, null, null);
	}
	
	//Restituisce un RdfSpreadsheet che contiene un oggetto rdfDescription che contiene titolo e tabella dello spreadsheet
	public ElementList getFeed(String spreadsheetName, String author, String webRoot, String serverAccount, String passwordAccount) throws AuthenticationException, MalformedURLException, IOException, ServiceException, URISyntaxException {
	    
		ElementList elementList = new ElementList();
		
		SpreadsheetService service = new SpreadsheetService("MySpreadsheetIntegration-v1");
		service.setUserCredentials(serverAccount, passwordAccount);
//		service.setUserCredentials("stefano.serra23@gmail.com", "temporanea1234");
	    // Define the URL to request.  This should never change.
	    URL SPREADSHEET_FEED_URL = new URL("https://spreadsheets.google.com/feeds/spreadsheets/private/full");

	    // Make a request to the API and get all spreadsheets.
	    SpreadsheetFeed feed = service.getFeed(SPREADSHEET_FEED_URL, SpreadsheetFeed.class);
	    List<SpreadsheetEntry> spreadsheets = feed.getEntries();

	    if (spreadsheets.size() == 0) {
	    	System.out.println("Non ci sono spreadsheets");
	    }

	    SpreadsheetEntry spreadsheetFound = null;
	    
	    for (SpreadsheetEntry feedElement : spreadsheets) {
	    	String authorFeed = feedElement.getAuthors().get(0).getName();//Autore relativo agli sheet ciclati
	    	String titleFeed = feedElement.getTitle().getPlainText();//titolo relativo agli sheet ciclati
	    	if(titleFeed.equalsIgnoreCase(spreadsheetName)){
	    		if(author != null && author.equalsIgnoreCase(authorFeed)){
	    			spreadsheetFound = feedElement;
	    		}else if(author == null){
	    			spreadsheetFound = feedElement;
	    		}
	    	}
		}
	    
	    if(spreadsheetFound == null){
	    	return null;
	    }
	    
	    // Get the first worksheet of the first spreadsheet.
	    WorksheetFeed worksheetFeed = service.getFeed(spreadsheetFound.getWorksheetFeedUrl(), WorksheetFeed.class);
	    List<WorksheetEntry> worksheets = worksheetFeed.getEntries();
	    
	    int i = 0;
	    WorksheetEntry worksheet = worksheets.get(i);

	    //Qui si aggiungono le indicazioni sulle parti di tabella da estrarre
	    // Fetch the cell feed of the worksheet.
	    URL cellFeedUrl = new URI(worksheet.getCellFeedUrl().toString() + "?min-row=1&min-col=1&max-col=2").toURL();
	    CellFeed cellFeed = service.getFeed(cellFeedUrl, CellFeed.class);
	 
	    //Carica nella Lista i dati estratti dalle prime 2 colonne dello spreadsheet
	    // Iterate through each cell, printing its value.
	    
	    List<XMLEntry> dataInput = new ArrayList<XMLEntry>();
	    
	    int cellIndex = 0;
	    String lastKey = "";
	    String lastValue = "";
	    for (CellEntry cell : cellFeed.getEntries()) {
	    	XMLEntry xmlEntry = new XMLEntry();
	    	boolean isNewRow = ((cellIndex % 2) == 0) ? true : false;
	    	if(isNewRow){
	    		lastKey = cell.getCell().getInputValue();
	    	} else {
	    		lastValue = cell.getCell().getInputValue();
	    	
	    	if(lastKey.startsWith("http")){
	    		int lastSlash = lastKey.lastIndexOf("/");
	    		String A = lastKey.substring(0, lastSlash);
	    		String B = lastKey.substring(lastSlash);
	    		xmlEntry.propertyNS = A;
	    		xmlEntry.propertyValue = B;
	    		xmlEntry.value = lastValue;
	    	}
	    	else{
	    		String A = webRoot + author;
	    		String B = "/" + lastKey;
	    		xmlEntry.propertyNS = A;
	    		xmlEntry.propertyValue = B;
	    		xmlEntry.value = lastValue;
	    	}
	    	
	    	dataInput.add(xmlEntry);
	    	}
	    	
	    	if(isNewRow){
	    	  cellIndex++;
	    	} else {
	    	  cellIndex = 0;
	    	}
	    }
	    
	    elementList.setAuthor(author);
	    elementList.setElements(dataInput);
	    elementList.setTitle(spreadsheetName);

	    return elementList;

	}

}