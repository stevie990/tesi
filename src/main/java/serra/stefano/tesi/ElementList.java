package serra.stefano.tesi;

import java.util.ArrayList;
import java.util.List;

public class ElementList {
	private List<XMLEntry> elements = new ArrayList<XMLEntry>();
	
	private String title;
	private String author;
	
	public void setElements(List<XMLEntry> elements) {
		this.elements = elements;
	}
	
	public List<XMLEntry> getElements() {
		return elements;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

}
